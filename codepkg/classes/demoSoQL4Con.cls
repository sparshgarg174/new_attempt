public with sharing class demoSoQL4Con {

     public String leadStatus { get; set; }
    public List<Account> listAccount {get; set;}
    public Integer accountCount { get; set; }
    
    public List<selectOption> ratingOpts = new List<selectOption>();
    public List<selectOption> getRatingOpts() {
        ratingOpts.clear();
        ratingOpts.add(new selectOption('default','--- None ---'));
      
        schema.describeFieldResult ratingDescription = Account.fields.Rating.getDescribe();
        List<PickListEntry> ratingEntry = ratingDescription.getPickListValues();
        for(PickListEntry entry : ratingEntry) {
            ratingOpts.add(new selectOption(entry.getValue(), entry.getLabel()));
        } 
        
//        ratingOpts.add(new selectOption('','unrated'));
        return ratingOpts;
    }
    
    public void fetchAccount() {
        accountCount =  [ select count() from Account where Rating =: leadStatus LIMIT 100 ];
        listAccount = [ select Name, Rating from Account where Rating =: leadStatus LIMIT 100 ];
    }
}