public class ApexforLightining {

    @auraEnabled
    Public STATIC List<String> returnContactNames()
    {
        List<String> conNames = new List<String>();
        List<Contact> sparshCons = [select Id,name from Contact LIMIT 500];
        for(Contact sparshCon : sparshCons)
        {
		
            conNames.add(sparshCon.Name);
        }
        return conNames;
    }
}