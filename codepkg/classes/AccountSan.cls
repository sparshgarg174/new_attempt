public class AccountSan {
 @auraEnabled
    public STATIC Account retAccount()
    {
        return [select Id,Name,Industry,Rating FROM Account LIMIT 10];
    }
    
    @auraEnabled
    public STATIC List<Account> selectTenAccount()
    {
        return [select Id,Name,Industry,Rating FROM Account LIMIT 10];
    
}
}
