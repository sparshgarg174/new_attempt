public with sharing class pageSizeExt {

    public pageSizeExt(ApexPages.StandardSetController exten) {

        exten.setPageSize(1);
    }

}