public class ApexLightningCon {
	  @auraEnabled
    Public STATIC String serverEcho(String firstName){
        return('Your name would be: '+ firstName);
    }
    @auraEnabled
    Public STATIC String returnIndianStyleName(String firstName, String lastName){
        return('Your full name in Indian style : '+ firstName+lastName);
        
    }
}