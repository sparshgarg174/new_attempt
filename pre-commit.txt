#!C:/Program\ Files/Git/bin/sh.exe

echo " "
echo "Before commit check for following prerequisites: "
echo "	1)Have the Test Classes been created for the functionality developed?"
echo "	2)Have the post deployment steps been mentioned in separate document?"
echo " "
echo "Do you wish to proceed with the commit [y/n]"
read doit < /dev/tty
echo " "

case $doit in  
  y|Y) echo "	Proceeding with commit" ;; 
  n|N) echo "	Aborting Commit"
		exit 1;; 
  *) echo "	Invalid Input, Aborting Commit"
		exit 1;; 
esac